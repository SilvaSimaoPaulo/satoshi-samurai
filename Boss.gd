extends KinematicBody2D


const GRAVITY: float = 650.0
const FLOOR: int = 560
export var speed: float = 300
export var jump_speed: float = -400.0
export var velocity: Vector2


# Atualiza a velocidade vertical
func jump(condition: bool) -> bool:
	if condition:
		velocity.y = jump_speed
		#$Jumping.play()
		return true
	else:
		return false


# Called when the node enters the scene tree for the first time.
func _ready():
	velocity = Vector2(0, 0)
	position = Vector2(500, FLOOR)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#pass
	var player_position: Vector2 = get_parent().get_node("Player").position
	var velocity_direction: Vector2 = player_position - position
	velocity.x = velocity_direction.normalized().x * speed
	if not jump(velocity_direction.y < 0 and velocity.y == 0):
		if position.y < FLOOR:
			velocity.y += GRAVITY * delta
	
	#print(position.y)
	# Seleciona animation
	if velocity.x < 0:
		$Li.flip_h = true
	else:
		$Li.flip_h = false
	if velocity.y != 0 and position.y < FLOOR:
		$Li.animation = "on_jump"
	elif velocity.x != 0:
		$Li.animation = "walk"
	else:
		$Li.animation = "alert"
	
	move_and_slide(velocity)
	$Li.play()

	# Evita que o sprite "caia" da tela
	if position.y > FLOOR:
		position.y = FLOOR
		velocity.y = 0
