extends Node2D


export (PackedScene) var Boss
export (PackedScene) var Player

# Called when the node enters the scene tree for the first time.
func _ready():
	var Li = Boss.instance()
	var Samurai = Player.instance()
	add_child(Samurai)
	add_child(Li)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
