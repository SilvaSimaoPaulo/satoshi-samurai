# Satoshi, o Samurai

### Ficha do jogo
* NOME DA EMPRESA DESENVOLVEDORA: TE190

* NOME DA EMPRESA DISTRIBUIDORA: --

* NOME DO JOGO: Satoshi, o Samurai

* CATEGORIA: Beat 'em up

* APRESENTAÇÃO DO JOGO (RESUMO):

* APRESENTAÇÃO COMPLETA:

* JOGABILIDADE: movimentação com as teclas WAD ou o direcional do teclado e ataque com espaço ou o botão esquerdo do mouse.

* COMANDOS, ATALHOS:

* SOFTWARES UTILIZADOS: Godot 3.2, Gimp, ImageMagick

* RELAÇÃO DE ELEMENTOS E SOFTWARES UTILIZADOS: ??

* ONDE BAIXAR: [disponível no Gitlab](https://gitlab.com/SilvaSimaoPaulo/satoshi-samurai)

* VALOR DE CUSTO: gratuito

* CREDITOS:
	* [Jumping and landing sounds](https://opengameart.org/content/jump-landing-sound);
	* [Knife sharpening sound](https://opengameart.org/content/knife-sharpening-slice-2);
	* [8-bit jump] (https://opengameart.org/content/8-bit-jump-1);
	* [Samurai sprites](https://www.gamedevmarket.net/asset/fantasy-heroes-samurai-sprite-sheet/)

