#extends Area2D
extends KinematicBody2D

const GRAVITY: float = 650.0
const FLOOR: int = 560
export var speed: float = 400.0
export var jump_speed: float = -500.0
export var velocity: Vector2


# Atualiza a velocidade vertical
func jump(condition: bool) -> bool:
	if condition:
		velocity.y = jump_speed
		if not $Jumping.is_playing():
			$Jumping.play()
		return true
	else:
		return false


# Called when the node enters the scene tree for the first time.
func _ready():
	velocity = Vector2(0, 0) # Inicializa velocity
	position = Vector2(100, 420)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	# Atualiza a velocidade e o lado do sprite
	if Input.is_action_pressed("ui_left"):
		velocity = Vector2(-speed, velocity.y)
		$Satoshi.flip_h = true
	if Input.is_action_pressed("ui_right"):
		velocity = Vector2(speed, velocity.y)
		$Satoshi.flip_h = false
	if not jump(Input.is_action_pressed("ui_up") and position.y == FLOOR):
		if position.y < FLOOR:
			velocity.y += GRAVITY * delta
	
	# Escolhe a animacao
	if Input.is_action_pressed("ui_accept"):
		$Satoshi.animation = "attack"
		if not $Sword.is_playing():
			$Sword.play()
	elif velocity.y != 0 and position.y < FLOOR:
		$Satoshi.animation = "on_jump"
	elif velocity.x != 0:
		$Satoshi.animation = "walk"
	else:
		$Satoshi.animation = "alert"

	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = 0.25 * velocity.bounce(collision.normal)		
		if jump(Input.is_action_pressed("ui_up") and (collision.normal.y < 0.5)):
			move_and_slide(velocity)
	
	# Evita que o sprite "caia" da tela
	if position.y > FLOOR:
		position.y = FLOOR
		velocity.y = 0
		$Landing.play()
	
	$Satoshi.play()
	velocity.x = 0 # Zera a velocidade x
	
